﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace SQL_Connection_Reader_StrictlyTyped
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create Connect Stream String
            string conStr = @"Data Source = .\SQLEXPRESS; Initial Catalog = ShopDB; Integrated Security = true;";

            //Create SQL Connection 
            SqlConnection connection = new SqlConnection(conStr);

            //Open Connection
            connection.Open();

            //Create SQL Command
            SqlCommand cmd = new SqlCommand("SELECT * FROM Custumers" + connection);

            //Create SQL Reader 
            SqlDataReader reader = cmd.ExecuteReader();

            //Reads all lines    Read()  method 
            while (reader.Read())
            {

                //displaying client ID using the method GetFieldValue
                Console.WriteLine(reader.GetFieldValue<int>(0));

                //displaying clinet FNameSurename
                Console.WriteLine(reader.GetString(2),reader.GetString(1),reader.GetString(3));

                //displaying client phone number
                Console.WriteLine(reader.GetFieldValue<string>(7));

                //displaying DataInSystem Client
                Console.WriteLine("{0:D}",reader.GetDateTime(8));
                //Console.WriteLine(reader.GetFieldValue<DateTime>(8));

                Console.WriteLine(new string('-',20));
            }
            //Close Reader
            reader.Close();

            //Close Connection
            connection.Close();

        }
    }
}
